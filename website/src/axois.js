import axios from 'axios';
import requests from './requests';


// we can also configure x-headers data into below configuration settings
const instance = axios.create({
    baseURL: 'https://rquero.herokuapp.com'
})


export default instance;