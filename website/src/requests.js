const requests = {
    getCourses: "/getcourse",
    searchCourses: "/searchcourse",
    signup: "/user/signup",
    login: "/user/login",
    logout: "/user/logout"
}

export default requests;