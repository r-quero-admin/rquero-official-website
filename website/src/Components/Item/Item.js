import React, { useEffect, useState } from "react";
import './Item.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faStar, faClock, faCalendar, faHome, faUsers, faTimes, faShare } from '@fortawesome/free-solid-svg-icons';
import Box from '@mui/material/Box';
import Popper from '@mui/material/Popper';
import Fade from '@mui/material/Fade';
import Paper from '@mui/material/Paper';
import { PaperProps } from "@mui/material";
import { makeStyles } from "@material-ui/core/styles";
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

function Item(props){
    console.log(props)
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [open, setOpen] = React.useState(false);
    const [placement, setPlacement] = React.useState();
    const [snackOpen, setSnackOpen] = React.useState(false);
    
    const Alert = React.forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });
    
    const handleClick = (newPlacement) => (event) => {
        event.stopPropagation();
        setAnchorEl(event.currentTarget);
        setOpen((prev) => placement !== newPlacement || !prev);
        setPlacement(newPlacement);
      };
      const handlePopperOpen = (newPlacement) => (event) => {
        event.stopPropagation();
        setAnchorEl(event.currentTarget);
        setOpen(true);
        setPlacement(newPlacement);
      }

      const handlePopperClose = (event) => {
        event.stopPropagation();
        setAnchorEl(null);
        setOpen(false);
      };

      const copyLinkToClipboard = (event) => {
          event.stopPropagation();
          setSnackOpen(true);
          console.log("Link Copied");
      }

      const handleSnackClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setSnackOpen(false);
      };

    return(
        <React.Fragment>
            <Box className="course-wrapper box-wrapper">
                <div className="row-box course" onClick={handleClick("auto")}>
                {/* onMouseEnter={handlePopperOpen('auto')} onMouseLeave={handlePopperClose} */}
                        <p className="course-provider provider-label">{props.provider}</p>
                    <div className="course-image">
                        <img src="./images/courses/test-image.jpg" />
                    </div>
                    <div className="course-details">
                        <p className={`course-price price-tag ${props.paid === "true" ? "paid" : "free"}`}>
                            {
                                props.paid === "true" ?
                            <span>&#8377; {props.price}</span>
                            :
                            "Free"
                            }
                        </p>
                        <button className="share-link popper-close" onClick={copyLinkToClipboard}>
                            <FontAwesomeIcon icon={faShare} />
                        </button>
                        <h1 className="course-title">Python - Advanced Concepts</h1>
                        <p className="course-description">
                            Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                        </p>
                        {/* <button className="description-expand">See full description</button> */}
                        <div className="course-metrics">
                            <div className="ratings-duration">
                                <div className="course-ratings">
                                    <span className="ratings-number">5</span>
                                    <span className="ratings-starts">
                                        <FontAwesomeIcon icon={faStar} />
                                        <FontAwesomeIcon icon={faStar} />
                                        <FontAwesomeIcon icon={faStar} />
                                        <FontAwesomeIcon icon={faStar} />
                                        <FontAwesomeIcon icon={faStar} />
                                    </span>
                                </div>
                                <div className="course-duration">
                                    <span><FontAwesomeIcon icon={faClock} /></span>
                                    <span className="duration">1.2 Hrs</span>
                                </div>
                            </div>
                            {/* <div className="course-university">University of California</div> */}
                            <div className="course-faculties">James Potter, Hallow Unilever, Peter Sedwick</div>
                        </div>
                    </div>
                    <div className="action-buttons">
                        <button className="course-link">Let's Go</button>
                    </div>
                </div>
                <Popper
                className='course-popper'
                open={open}
                anchorEl={anchorEl}
                placement={placement}
                onMouseLeave={handlePopperClose}
                transition
                >
                    {({ TransitionProps }) => (
                    <Fade {...TransitionProps} timeout={350}>
                        <div className="course-details-popbox">
                            <div className="popper-close">
                                <FontAwesomeIcon icon={faTimes} onClick={handlePopperClose} />
                            </div>
                            <h1 className="course-title pop-title">Python - Advanced Concepts</h1>
                            <div className="hype-date-wrapper">
                                <p className="hype-tag">Bestseller</p>
                                <p className="course-date-details">
                                    <FontAwesomeIcon icon={faCalendar} />
                                    <span>Updated November, 2021</span>
                                </p>
                            </div>
                            <div className="categories">
                                Programming | Coding | Data Structures
                            </div>
                            <p className="levels">All Levels</p>
                            <p className="course-description pop-description">
                                Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                                <br/><br/>
                                Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                                <br/>
                                <br/>
                                Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                                <br/><br/>  
                                Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                            </p>
                            <div className="course-university pop-university">
                                <FontAwesomeIcon icon={faHome} />
                                <span>University of California</span>
                            </div>
                            <div className="course-faculties pop-faculties">
                                <FontAwesomeIcon icon={faUsers} />
                                <span>James Potter, Hallow Unilever, Peter Sedwick</span>
                            </div>
                            <div className="action-buttons">
                                <button className="course-link">Let's Go</button>
                                <div className="favourites-wrapper">
                                    <FontAwesomeIcon className="favourite" icon={faHeart} />
                                </div>
                            </div>
                        </div>
                    </Fade>
                    )}
                </Popper>
        </Box>
       
        <Snackbar className="page-snack-message" open={snackOpen} autoHideDuration={1000} onClose={handleSnackClose}>
            <Alert onClose={handleSnackClose} severity="success" sx={{ width: '100%' }}>
                Link Copied Successfully!
            </Alert>
        </Snackbar>
        </React.Fragment>

    )
}

export default Item;