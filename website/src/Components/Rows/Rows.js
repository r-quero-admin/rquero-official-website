import React, { useEffect, useState } from "react";
import './Rows.scss';
import Item from "../Item/Item";
import axios from '../../axois';
import requests from "../../requests";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faStar, faClock, faCalendar, faHome, faUsers, faTimes, faShare } from '@fortawesome/free-solid-svg-icons';
import Box from '@mui/material/Box';
import Popper from '@mui/material/Popper';
import Fade from '@mui/material/Fade';
import Paper from '@mui/material/Paper';
import { PaperProps } from "@mui/material";
import { makeStyles } from "@material-ui/core/styles";
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

function Rows(props){ 
    const [courses, setCourses] = useState([]);
    const [anchorEl, setAnchorEl] = useState(null);
    const [open, setOpen] = useState(false);
    const [placement, setPlacement] = useState();
    const [snackOpen, setSnackOpen] = useState(false);
    const [wishlisht, setWishlist] = useState(false);

    useEffect(()=>{
        async function fetchCourses(){
            const request = await axios.get(props.fetchUrl)
            setCourses(request.data);
        }

        fetchCourses();
    }, [props.fetchUrl])

    const addToWishlisht = (event) => {
        setWishlist(!wishlisht);
    }

    const Alert = React.forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });
    
    const handleClick = (newPlacement) => (event) => {
        event.stopPropagation();
        setAnchorEl(event.currentTarget);
        setOpen((prev) => placement !== newPlacement || !prev);
        setPlacement(newPlacement);
      };
      const handlePopperOpen = (newPlacement) => (event) => {
        event.stopPropagation();
        setAnchorEl(event.currentTarget);
        setOpen(true);
        setPlacement(newPlacement);
      }

      const handlePopperClose = (event) => {
        event.stopPropagation();
        setAnchorEl(null);
        setOpen(false);
      };

      const copyLinkToClipboard = (event, copylink) => {
          event.stopPropagation();
          setSnackOpen(true);
          console.log("Link Copied");
          console.log(copylink);
          navigator.clipboard.writeText(copylink);
      }

      const handleSnackClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setSnackOpen(false);
      };

    return(
        <React.Fragment>
        <div className="row">
            <h1 className="platform-course-header">{props.heading}</h1>
            <div className="course-row-poster">
                {
                    courses.map(function(course, index){
                        console.log(course)
                        return(
                            <Box className="course-wrapper box-wrapper">
                                <div className="row-box course" onClick={handleClick("auto")}>
                                    <p className="course-provider provider-label">{course.platform.toUpperCase()}</p>
                                    <div className="course-image">
                                        <img src={course.course_thumbnail_image} />
                                    </div>
                                    <div className="course-details">
                                        <p className={`course-price price-tag ${course.is_paid === true ? "paid" : "free"}`}>
                                            {
                                                course.is_paid === true ?
                                                // <span>&#8377; {props.price}</span>
                                                <span>{course.currency_symbol}&nbsp;{course.amount}</span>
                                                :
                                                "Free"
                                            }
                                        </p>
                                        <button className="share-link popper-close" onClick={(event)=>{
                                            copyLinkToClipboard(event, course.baseurl + course.url)
                                        }}>
                                            <FontAwesomeIcon icon={faShare} />
                                        </button>
                                        <h1 className="course-title">{course.title}</h1>
                                        <p className="course-description">
                                            {course.tags[0]}
                                        </p>
                                        {/* <button className="description-expand">See full description</button> */}
                                        <div className="course-metrics">
                                            <div className="ratings-duration">
                                                <div className="course-ratings">
                                                    <span className="ratings-number">5</span>
                                                    <span className="ratings-starts">
                                                        <FontAwesomeIcon icon={faStar} />
                                                        <FontAwesomeIcon icon={faStar} />
                                                        <FontAwesomeIcon icon={faStar} />
                                                        <FontAwesomeIcon icon={faStar} />
                                                        <FontAwesomeIcon icon={faStar} />
                                                    </span>
                                                </div>
                                                {
                                                    course.duration ?
                                                    <div className="course-duration">
                                                        <span><FontAwesomeIcon icon={faClock} /></span>
                                                        <span className="duration">{course.duration}</span>
                                                    </div>
                                                    :
                                                    ""
                                                }
                                            </div>
                                            {/* <div className="course-university">University of California</div> */}
                                            {
                                                course.instructor ?
                                                <div className="course-faculties">
                                                    {
                                                        course.instructor.map(function(instructor, index){
                                                            return(
                                                                <span>{instructor.display_name}{index < course.instructor.length-1 ? " | " : ''}</span>
                                                            )
                                                        })
                                                    }
                                                </div>
                                                :
                                                "No Faculties Mentioned"
                                            }
                                        </div>
                                    </div>
                                    <div className="action-buttons">
                                        <a className="course-link" target="_blank" href={course.baseurl+course.url}>Let's Go</a>
                                    </div>
                                </div>
                                <Popper
                                className='course-popper'
                                open={open}
                                anchorEl={anchorEl}
                                placement={placement}
                                // onMouseLeave={handlePopperClose}
                                transition
                                >
                                    {({ TransitionProps }) => (
                                    <Fade {...TransitionProps} timeout={350}>
                                        <div className="course-details-popbox">
                                            <div className="popper-close">
                                                <FontAwesomeIcon icon={faTimes} onClick={handlePopperClose} />
                                            </div>
                                            <h1 className="course-title pop-title">Python - Advanced Concepts</h1>
                                            <div className="hype-date-wrapper">
                                                <p className="hype-tag">Bestseller</p>
                                                <p className="course-date-details">
                                                    <FontAwesomeIcon icon={faCalendar} />
                                                    <span>Updated November, 2021</span>
                                                </p>
                                            </div>
                                            <div className="categories">
                                                Programming | Coding | Data Structures
                                            </div>
                                            <p className="levels">All Levels</p>
                                            <p className="course-description pop-description">
                                                Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                                                <br/><br/>
                                                Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                                                <br/>
                                                <br/>
                                                Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                                                <br/><br/>  
                                                Advanced concepts of Python including functions, decorators, web app creation, magic methods and many more.
                                            </p>
                                            <div className="course-university pop-university">
                                                <FontAwesomeIcon icon={faHome} />
                                                <span>University of California</span>
                                            </div>
                                            <div className="course-faculties pop-faculties">
                                                <FontAwesomeIcon icon={faUsers} />
                                                <span>James Potter, Hallow Unilever, Peter Sedwick</span>
                                            </div>
                                            <div className="action-buttons">
                                                <a className="course-link">Let's Go</a>
                                                <div className="favourites-wrapper">
                                                    <FontAwesomeIcon className={`favourite ${wishlisht ? 'active' : ''}`} icon={faHeart} onClick={addToWishlisht} />
                                                </div>
                                            </div>
                                        </div>
                                    </Fade>
                                    )}
                                </Popper>
                            </Box>
                                )
                            })
                        }
            </div>
        </div>
        <Snackbar className="page-snack-message" open={snackOpen} autoHideDuration={1000} onClose={handleSnackClose}>
            <Alert onClose={handleSnackClose} severity="success" sx={{ width: '100%' }}>
                Link Copied Successfully!
            </Alert>
        </Snackbar>
        </React.Fragment>
    )
}

export default Rows;