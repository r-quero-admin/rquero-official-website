import React from "react";
import { Link } from 'react-router-dom'
import './Header.scss';

function Header(){
    return(
        <div className="header">
            <div className="logo">
                <h1>rQuero</h1>
            </div>
            <div className="header-links">
                <Link to="/aboutus/">About Us</Link>
                <Link to="/blog">Blog</Link>
            </div>
        </div>
    )
}

export default Header;