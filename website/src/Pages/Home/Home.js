import React from "react";
import Rows from '../../Components/Rows/Rows'
import Header from "../../Components/Header/Header";
import requests from "../../requests";
import './Home.scss'

function Home(){
    return(
        <React.Fragment>
            <Header />
            <div className="contents">
                <Rows heading="Featured Courses" fetchUrl={requests.getCourses} />
                <Rows heading="Trending Courses" fetchUrl={requests.getCourses}  />
            </div>
        </React.Fragment>
    )
}

export default Home;